package com.salesmanager.web.shop.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

@Controller
@RequestMapping("/shop/testpage")
public class TestPageController {
    @RequestMapping(method = RequestMethod.GET)
    public String insertProduct(Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection;
            Statement stmt;
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/salesmanager", "carlito", "ninetrails");
            stmt = connection.createStatement();

            stmt.executeUpdate("INSERT INTO  PRODUCT" + "(PRODUCT_ID, AVAILABLE, PRODUCT_FREE, PRODUCT_SHIP, PRODUCT_VIRTUAL,  SKU, SORT_ORDER, MANUFACTURER_ID, MERCHANT_ID, TAX_CLASS_ID, PRODUCT_TYPE_ID) " + "VALUES"
                    + "(2, 2, 0, 0, 0, '1B0', 0, 1, 1, 1, 1)");

            stmt.executeUpdate("INSERT INTO PRODUCT_AVAILABILITY" + "(PRODUCT_AVAIL_ID, QUANTITY, QUANTITY_ORD_MAX, QUANTITY_ORD_MIN, PRODUCT_ID)" + "VALUES " + "(2, 2, 1, 2, 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_DESCRIPTION" + "(DESCRIPTION_ID, DESCRIPTION, NAME, META_TITLE, SEF_URL, LANGUAGE_ID, PRODUCT_ID)" + "VALUES " + "(802, '<table border=\"0\" class=\"ggg5\" style=\"margin: 0px; padding: 0px; color: rgb(72, 71, 72); font-family: Arial, Helvetica, sans-serif; font-size: 13.3333330154419px; background-color: rgb(255, 255, 255);\" width=\"100%\">\n" +
                    "\t<tbody style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t<tr style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t<td class=\"ggg6\" style=\"margin: 0px; padding: 0px 30px; width: 380px;\" valign=\"top\">\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Дисплей</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\t5.1-дюймовый Quad HD Super AMOLED (1440x2560) 577 PPI</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Процессор</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\t8-ядерный процессор: 4 ядра на 1.3 GHz Cortex-A53, 4 ядра на 1.9 GHz Cortex-A57</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tГрафический процессор: Mali-T760</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Память</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tОперативная память: 3 Гб DDR4</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tВстроенная память: 32 Гб</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tПоддержка карт отсутствует</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Сеть</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tGSM 850 / 900 / 1800 / 1900 МГц</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tHSDPA 850 / 900 / 1900 / 2100 МГц</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tLTE</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Камера</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tОсновная камера: 16.0 Мп (3456 x 4608 px)</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tФронтальная камера: 5 Мп</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tРазрешение записи видео: 4K</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tВспышка, автофокус</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tОптическая стабилизация изображения</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Особенности</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tСканер отпечатков пальцев</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tБеспроводная зарядка</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tУправление голосом и голосовой ввод</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tУмная подсветка, умная пауза, умная прокрутка</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tУправление жестами</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t&nbsp;</h2>\n" +
                    "\t\t\t</td>\n" +
                    "\t\t\t<td class=\"ggg4\" style=\"margin: 0px; padding: 0px 30px 0px 40px;\" valign=\"top\">\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Операционная система и оболочка</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tAndroid 5.0.2 (Lollipop)</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tОболочка TouchWiz UI</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Подключения</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tWi-Fi 802.11 a/b/g/n/ac 2.4G+5GHz</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tWi-Fi Direct</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tBluetooth 4.0</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tСистема навигации: GPS, Glonass, Beidou</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tNFC</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tANT+</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tMHL</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tUSB 3.0</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tИнфракрасный порт</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tРазъем для наушников: 3.5 мм стерео</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Датчики</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tДатчик поворота экрана, Барометр, Сканер отпечатков пальцев, Гироскоп, Геомагнетический датчик, Управление жестами, Датчик Холла, Пульсометр, Датчик приближения, RGB-датчик, Датчик ультрафиолета</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Аккумулятор</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\t2550 мАч Li-Ion</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t\t<h2 style=\"margin: 10px 0px; padding: 0px 5px 0px 0px; font-family: ubuntu; color: black; font-size: 14px; max-height: 104px;\">\n" +
                    "\t\t\t\t\t<font size=\"3\" style=\"margin: 0px; padding: 0px;\">Физические характеристики</font></h2>\n" +
                    "\t\t\t\t<ul style=\"margin: 0px; padding: 0px;\">\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tРазмеры (В х Ш х Г): 143.4 x 70. x 6.8 мм</li>\n" +
                    "\t\t\t\t\t<li style=\"margin: 0px 0px 0px 18px; padding: 0px;\">\n" +
                    "\t\t\t\t\t\tВес: 138 г</li>\n" +
                    "\t\t\t\t</ul>\n" +
                    "\t\t\t</td>\n" +
                    "\t\t</tr>\n" +
                    "\t</tbody>\n" +
                    "</table>\n" +
                    "<p>\n" +
                    "\t&nbsp;</p>\n', 'Samsung Galaxy S6', 'Samsung Galaxy S6', 'samsung-galaxy-s6', 1, 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_IMAGE" + "(PRODUCT_IMAGE_ID, DEFAULT_IMAGE, IMAGE_CROP, IMAGE_TYPE, PRODUCT_IMAGE, PRODUCT_ID)" + "VALUES " + "(2, 1, 0, 0, 'batllo-0853.jpg', 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_IMAGE_DESCRIPTION" + "(DESCRIPTION_ID, DESCRIPTION, NAME, LANGUAGE_ID, PRODUCT_IMAGE_ID)" + "VALUES " + "(902, null, 'batllo-0853.jpg', 1, 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_PRICE" + "(PRODUCT_PRICE_ID, PRODUCT_PRICE_CODE, DEFAULT_PRICE, PRODUCT_PRICE_AMOUNT, PRODUCT_PRICE_TYPE, PRODUCT_AVAIL_ID)" + "VALUES " + "(2, 'base', 1, 18000.00,  'ONE_TIME', 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_PRICE_DESCRIPTION" + "(DESCRIPTION_ID, DESCRIPTION, NAME, LANGUAGE_ID, PRODUCT_PRICE_ID)" + "VALUES " + "(950, null, 'DEFAULT', 1, 2)");

            stmt.executeUpdate("INSERT INTO PRODUCT_RELATIONSHIP" + "(PRODUCT_RELATIONSHIP_ID, ACTIVE, CODE, RELATED_PRODUCT_ID, MERCHANT_ID)" + "VALUES " + "(2, 1, 'FEATURED_ITEM', 2, 1)");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();


        } catch (SQLException e) {

            e.printStackTrace();

        }
        return "testpage";
    }


}

